# README #

This applications has the base configuration and some samples on how to test using selenium web driver and cucumber.

* Version 1.0

### Instructions ###

* You will need:

- JAVA 8
- Eclipse IDE (http://www.eclipse.org/neon/)
- Install eclipse plugins: https://cucumber.io/cucumber-eclipse/
- Maven



### Branches ###
step1 - Contain the base configurations for the project, use this for new projects.